﻿// See https://aka.ms/new-console-template for more information

using System;
using System.Runtime.ConstrainedExecution;

namespace Sesion01
{


    class Program
    {
        public record Car(string Make, string Model, int Year);
        static void Main(string[] args)
        {
            //Car myCar = new Car("Toyota", "Camry", 2022);
            //Car myCar2 = new Car("Toyota", "Camry", 2022);

            //var isEquals = myCar.Equals(myCar2);

            //Console.WriteLine(isEquals);

            // TODO: Add your code here


            //dynamic myVar = 1;

            //Console.WriteLine( myVar.GetType());
            //myVar = "Hola";

            //Console.WriteLine(myVar.GetType());

            //var objValue = new
            //{
            //    Name = "Juan",
            //};


            //Console.WriteLine(objValue.GetType()); // System.Int32


            //int[] numbers = { 1, 2, 3, 4, 5 };

            //bool allEven = Array.TrueForAll(numbers, n => n % 2 == 0);

            //Console.WriteLine($"Are all numbers even? {allEven}");

            //// Crear una lista de números enteros
            //List<int> numeros = new List<int>();

            //// Agregar elementos a la lista
            //numeros.Add(10);
            //numeros.Add(20);
            //numeros.Add(30);

            //// Acceder al primer elemento de la lista
            //int primerElemento = numeros[0]; // primerElemento = 10

            //// Modificar un elemento de la lista
            //numeros[1] = 25;

            //// Eliminar un elemento de la lista
            //numeros.Remove(30);


            //// Iterar sobre los elementos de la lista usando un bucle foreach
            //Console.WriteLine("Elementos de la lista:");
            //foreach (int numero in numeros)
            //{
            //    Console.WriteLine(numero);
            //}

            // Crear una lista de strings con una capacidad inicial grande
            List<string> palabras = new List<string>(10);
            palabras.Add("Hola");
            palabras.Add("Mundo");
            palabras.Add("!");

            Console.WriteLine("Capacidad inicial de la lista: " + palabras.Capacity);
            Console.WriteLine("Número de elementos en la lista: " + palabras.Count);

            // Reducir la capacidad interna al número de elementos actuales
            palabras.TrimExcess();

            Console.WriteLine("Capacidad después de TrimExcess: " + palabras.Capacity);
        }
    }

    
    public enum EnumTest
    {
        Value1 = 1,
        Value2 = 2,
        Value3 = 3
    }
}
