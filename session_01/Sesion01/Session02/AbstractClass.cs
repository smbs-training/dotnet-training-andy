﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session02
{
    public abstract class Repository
    {
        public abstract void Add();
        public abstract void Update();
        public abstract void Delete();

        public void GetAll()
        {
            Console.WriteLine("Get All");
        }
    }

    class CustomerRepository : Repository
    {
        public override void Add()
        {
            Console.WriteLine("Add Customer");
        }

        public override void Update()
        {
            Console.WriteLine("Update Customer");
        }

        public override void Delete()
        {
            Console.WriteLine("Delete Customer");
        }
    }
}
