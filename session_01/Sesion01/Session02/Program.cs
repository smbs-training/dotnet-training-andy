﻿
using Session02;
using Session02.MathUtils;

internal class Program
{
    private static void Main(string[] args)
    {
        //int result1 = MathUtils.Add(1, 2);
        //int result2 = MathUtils.Subtract(1, 2);
        //int result3 = MathUtils.Multiply(1, 2);
        //int result4 = MathUtils.Divide(2, 1);

        //Console.WriteLine($"Add: {result1}");
        //Console.WriteLine($"Subtract: {result2}");
        //Console.WriteLine($"Multiply: {result3}");
        //Console.WriteLine($"Divide: {result4}");

        //PartialClass partial = new PartialClass();
        //partial.Method1();
        //partial.Method2();
        //partial.Method3();

        CustomerRepository repository = new CustomerRepository();
        repository.Add();
        repository.Update();
        repository.Delete();
        repository.GetAll();
    }
}