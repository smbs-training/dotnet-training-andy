﻿using Session05.Models;

namespace Session05
{
    public interface ILogin
    {
        Task<string> Login(LoginModel login);
    }
}
