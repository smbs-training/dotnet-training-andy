﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Session05;
using Session05.Models;
using Session05.Repository;

namespace Session05.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly AuthorRepository _repository;

        public AuthorsController(AuthorRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Authors
        [HttpGet]
        public async Task<ActionResult<List<Author>>> GetAuthors()
        {
            return await _repository.GetAuthors();
        }

        // GET: api/Authors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Author>> GetAuthor(int id)
        {
            //var author = await _context.Authors.FindAsync(id);
            var author = await _repository.GetAuthor(id);

            if (author == null)
            {
                return NotFound();
            }

            return author;
        }

        // PUT: api/Authors/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, Author author)
        {
            if (id != author.Id)
            {
                return BadRequest();
            }

            //_context.Entry(author).State = EntityState.Modified;
            await _repository.PutAuthor(id, author);

            //try
            //{
            //    await _context.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!AuthorExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return NoContent();
        }

        // POST: api/Authors
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Author>> PostAuthor(Author author)
        {
            //_context.Authors.Add(author);
            //await _context.SaveChangesAsync();
            await _repository.PostAuthor(author);

            return CreatedAtAction("GetAuthor", new { id = author.Id }, author);
        }

        // DELETE: api/Authors/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuthor(int id)
        {
            ////var author = await _context.Authors.FindAsync(id);
            //var author = await _repository.GetAuthor(id);
            //if (author == null)
            //{
            //    return NotFound();
            //}

            //_context.Authors.Remove(author);
            //await _context.SaveChangesAsync();

            await _repository.DeleteAuthor(id);

            return NoContent();
        }

        //private bool AuthorExists(int id)
        //{
        //    return _context.Authors.Any(e => e.Id == id);
        //}
    }
}
