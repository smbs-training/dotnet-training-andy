﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Session05.Models;

namespace Session05.Repository
{
    public class AuthorRepository
    {
        private readonly AppDbContext _context;

        public AuthorRepository(AppDbContext context)
        {
            _context = context;
        }

        //public async Task<List<Author>> GetAuthors()
        //{
        //    return await _context.Authors.Include(x => x.Books).ToListAsync();
        //}

        public async Task<List<Author>> GetAuthors()
        {
            var response = await _context.Database.SqlQueryRaw<AuthorViewModel>("dbo.sp_GetAuthors").ToListAsync();

            var ResponseData = response.Select(x => new Author
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return ResponseData;

            //var query = from author in _context.Authors
            //            join book in _context.Books on author.Id equals book.AuthorId into books
            //            select new Author
            //            {
            //                Id = author.Id,
            //                Name = author.Name,
            //                Books = books.ToList()
            //            };

            //return await query.ToListAsync();
        }

        public async Task<Author?> GetAuthor(int id)
        {
            return await _context.Authors.FindAsync(id);
        }

        public async Task<Author?> PostAuthor(Author author)
        {
            //_context.Authors.Add(author);
            //await _context.SaveChangesAsync();

            var response = await _context.Database.ExecuteSqlRawAsync("dbo.sp_InsertAuthor @Name = @Name",
                new SqlParameter("@Name", author.Name));

            return author;
        }

        public async Task<Author> PutAuthor(int id, Author author)
        {
            if (id != author.Id)
            {
                return null;
            }

            _context.Entry(author).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return author;
        }

        public async Task<Author?> DeleteAuthor(int id)
        {
            var author = await _context.Authors.FindAsync(id);
            if (author == null)
            {
                return null;
            }

            _context.Authors.Remove(author);
            await _context.SaveChangesAsync();

            return author;
        }

        private bool AuthorExists(int id)
        {
            return _context.Authors.Any(e => e.Id == id);
        }
    }
}
