﻿using Microsoft.IdentityModel.Tokens;
using Session05.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Session05.Repository
{
    public class LoginRepository : ILogin
    {
        private readonly IConfiguration _config;
        private readonly IUserRepository _userRepository;

        public LoginRepository(IConfiguration config, IUserRepository userRepository)
        {
            _config = config;
            _userRepository = userRepository;
        }

        public async Task<string> Login(LoginModel login)
        {
            var userFound = await _userRepository.GetUserByEmail(login.Email);
            if (userFound == null)
            {
                throw new Exception(login.Email);
            }

            var passwordIsValid = BCrypt.Net.BCrypt.EnhancedVerify(login.Password, userFound.Password);
            if (!passwordIsValid)
            {
                throw new Exception("Password incorrect");
            }

            var secretKey = _config.GetSection("Jwt:Key").Get<string>();

            if (string.IsNullOrEmpty(secretKey))
            {
                throw new Exception("Token configuration not found");
            }

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.GivenName, userFound.Name),
                new Claim(ClaimTypes.Email, userFound.Email)
            };

            //foreach (var role in userFound.Roles)
            //{
            //    claims.Add(new Claim(ClaimTypes.Role, role.Name));
            //}

            var tokenDesc = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = credentials
            };

            var token = tokenHandler.CreateToken(tokenDesc);

            return tokenHandler.WriteToken(token);
        }
    }
}
