﻿using Microsoft.EntityFrameworkCore;
using Session05.Models;

namespace Session05.Repository
{
    public class UserRespository : IUserRepository
    {
        private readonly IConfiguration _config;
        private readonly AppDbContext _context;
        public UserRespository(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _context = context;
        }

        public async Task<List<User>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User?> GetUserByEmail(string email)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
            return user;
        }

        public async Task CreateUser(User user)
        {
            var workFactor = _config.GetSection("BCrypt:WorkFactor").Get<int>();
            string passwordHash = BCrypt.Net.BCrypt.EnhancedHashPassword(user.Password, workFactor);
            user.Password = passwordHash; // password updated to hash

            await _context.Users.AddAsync(user);
            _context.SaveChanges();
        }

        public async Task<User> UpdateUser(User user)
        {
            var userFound = await _context.Users.FindAsync(user.Id);
            if (userFound == null)
            {
                throw new Exception("User not found");
            }

            if (user.Password != null)
            {
                var workFactor = _config.GetSection("BCrypt:WorkFactor").Get<int>();
                string passwordHash = BCrypt.Net.BCrypt.EnhancedHashPassword(user.Password, workFactor);
                user.Password = passwordHash; // password updated to hash
            }

            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return user;
        }
    }
}
