﻿using Session05.Models;

namespace Session05.Repository
{
    public interface IUserRepository
    {
        Task<List<User>> GetUsers();
        Task<User?> GetUserByEmail(string email);
        Task CreateUser(User user);
        Task<User> UpdateUser(User user);
    }
}
