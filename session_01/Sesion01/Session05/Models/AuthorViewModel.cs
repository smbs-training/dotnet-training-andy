﻿using System.ComponentModel.DataAnnotations;

namespace Session05.Models
{
    public class AuthorViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
