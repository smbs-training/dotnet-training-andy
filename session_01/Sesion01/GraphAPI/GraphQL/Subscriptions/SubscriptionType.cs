﻿namespace GraphAPI.GraphQL.Subscriptions
{
    public class SubscriptionType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor
                .AddBookSubscriptions();
        }
    }
}
