﻿using HotChocolate.Execution;
using HotChocolate.Subscriptions;

namespace GraphAPI.GraphQL.Subscriptions
{
    public static class BookSubscriptionType
    {
        public static void AddBookSubscriptions(this IObjectTypeDescriptor descriptor)
        {
            descriptor.Field("BookAdded")
                .Type<NonNullType<StringType>>()
                .Resolve(ctx => ctx.GetEventMessage<string>())
                .Subscribe(async ctx =>
                {
                    var receiver = ctx.Service<ITopicEventReceiver>();
                    ISourceStream stream = await receiver.SubscribeAsync<string>("BookAdded");

                    return stream;
                });
        }
    }
}
