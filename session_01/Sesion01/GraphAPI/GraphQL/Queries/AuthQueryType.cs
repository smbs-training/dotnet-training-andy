﻿using System.Security.Claims;

namespace GraphAPI.GraphQL.Queries
{
    public static class AuthQueryType
    {
        public static IObjectTypeDescriptor AddAuthQueries(this IObjectTypeDescriptor descriptor)
        {
            descriptor
            .Field("me")
            .Authorize()
            .Resolve(context =>
            {
                var claimsPrincipal = context.GetUser();

                var userId = claimsPrincipal?.FindFirstValue(ClaimTypes.GivenName);

                return userId;
            });

            return descriptor;
        }
    }
}
