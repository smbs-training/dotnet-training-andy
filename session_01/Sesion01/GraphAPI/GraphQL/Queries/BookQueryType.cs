﻿using GraphAPI.GraphQL.Types;
using GraphAPI.Repositories.Interfaces;
using HotChocolate.Types.Pagination;

namespace GraphAPI.GraphQL.Queries
{
    public static class BookQueryType
    {
        public static IObjectTypeDescriptor AddBookQueries(this IObjectTypeDescriptor descriptor)
        {
            descriptor
                .Field("book")
                .Type<BookType>()
                .Resolve(ctx => ctx.Service<IBookRepository>().GetBook());

            descriptor.Field("books")
                .Type<ListType<BookType>>()
                .UsePaging<BookType>(options: new PagingOptions
                {
                    MaxPageSize = 2,
                    IncludeTotalCount = true
                })
                .UseProjection()
                .UseFiltering()
                .UseSorting()
                .Resolve(ctx => ctx.Service<IBookRepository>().GetBooks());

            return descriptor;
        }


    }
}
