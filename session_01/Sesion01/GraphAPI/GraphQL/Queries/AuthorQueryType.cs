﻿using GraphAPI.GraphQL.Types;
using GraphAPI.Repositories.Interfaces;

namespace GraphAPI.GraphQL.Queries
{
    public static class AuthorQueryType
    {
        public static IObjectTypeDescriptor AddAuthorQueries(this IObjectTypeDescriptor descriptor)
        {
            descriptor
                .Field("author")
                .Type<AuthorType>()
                .Resolve(async ctx => await ctx.Service<IAuthorRepository>().GetAuthorAsync(1));

            descriptor
                .Field("authors")
                .Type<ListType<AuthorType>>()
                .Resolve(ctx =>
                {
                    var response = ctx.Service<IAuthorRepository>().GetAuthors();
                    return response;
                });

            return descriptor;
        }
    }
}
