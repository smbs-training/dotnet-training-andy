﻿namespace GraphAPI.GraphQL.Queries
{
    public class QueryType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor
                .AddBookQueries();

            descriptor
                .AddAuthQueries();

            descriptor.AddAuthorQueries();
        }
    }
}
