﻿using GraphAPI.Models;

namespace GraphAPI.GraphQL.Types
{
    public class CreateBookInputType : InputObjectType<Book>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Book> descriptor)
        {
            descriptor.Field(t => t.Id)
                .Type<IdType>();
            descriptor.Field(t => t.Title);
            descriptor.Field(t => t.Author)
                .Type<CreateAuthorInputType>();
        }
    }
}
