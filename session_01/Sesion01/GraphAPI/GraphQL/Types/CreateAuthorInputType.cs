﻿using GraphAPI.Models;

namespace GraphAPI.GraphQL.Types
{
    public class CreateAuthorInputType : InputObjectType<Author>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Author> descriptor)
        {
            descriptor.Field(t => t.Id).Type<IdType>();
            descriptor.Field(t => t.Name);
        }
    }
}
