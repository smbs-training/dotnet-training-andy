﻿using GraphAPI.Models;

namespace GraphAPI.GraphQL.Types
{
    public class LoginInputType : InputObjectType<LoginModel>
    {
        protected override void Configure(IInputObjectTypeDescriptor<LoginModel> descriptor)
        {
            descriptor.Field(t => t.Email);
            descriptor.Field(t => t.Password);
        }
    }
}
