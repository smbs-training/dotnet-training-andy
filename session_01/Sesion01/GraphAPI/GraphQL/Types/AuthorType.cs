﻿using GraphAPI.Models;

namespace GraphAPI.GraphQL.Types
{
    public class AuthorType : ObjectType<Author>
    {
        protected override void Configure(IObjectTypeDescriptor<Author> descriptor)
        {
            descriptor.Field(t => t.Name);
        }
    }
}
