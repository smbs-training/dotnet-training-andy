﻿namespace GraphAPI.GraphQL.Mutations
{
    public class MutationType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor
                .AddBookMutations();

            descriptor
                .AddAuthMutations();

            descriptor.AddAuthorMutations();
        }
    }
}
