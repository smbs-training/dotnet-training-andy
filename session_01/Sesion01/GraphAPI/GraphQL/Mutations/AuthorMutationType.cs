﻿using GraphAPI.GraphQL.Types;
using GraphAPI.Models;
using GraphAPI.Repositories.Interfaces;

namespace GraphAPI.GraphQL.Mutations
{
    public static class AuthorMutationType
    {
        public static IObjectTypeDescriptor AddAuthorMutations(this IObjectTypeDescriptor descriptor)
        {
            descriptor
                .Field("createAuthor")
                .Argument("author", a => a.Type<NonNullType<CreateAuthorInputType>>())
                //.Type<AuthorType>()
                .Type<StringType>()
                .Resolve(async ctx =>
                {
                    var author = ctx.ArgumentValue<Author>("author");
                    try
                    {
                        await ctx.Service<IAuthorRepository>().CreateAuthorAsync(author);
                        return "author";
                    }
                    catch (Exception ex)
                    {
                        throw new GraphQLException(ex.Message);
                    }
                });

            descriptor
                .Field("updateAuthor")
                .Argument("author", a => a.Type<NonNullType<CreateAuthorInputType>>())
                .Type<AuthorType>()
                .Resolve(async ctx =>
                {
                    var author = ctx.ArgumentValue<Author>("author");
                    await ctx.Service<IAuthorRepository>().UpdateAuthorAsync(author);
                    return author;
                });

            descriptor
                .Field("deleteAuthor")
                .Argument("id", a => a.Type<NonNullType<IntType>>())
                .Type<AuthorType>()
                .Resolve(async ctx =>
                {
                    var id = ctx.ArgumentValue<int>("id");
                    var author = await ctx.Service<IAuthorRepository>().GetAuthorAsync(id);
                    await ctx.Service<IAuthorRepository>().DeleteAuthorAsync(id);
                    return author;
                });

            return descriptor;
        }
    }
}
