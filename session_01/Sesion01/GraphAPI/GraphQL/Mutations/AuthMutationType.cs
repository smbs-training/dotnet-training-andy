﻿using GraphAPI.GraphQL.Types;
using GraphAPI.Models;
using GraphAPI.Repositories.Interfaces;

namespace GraphAPI.GraphQL.Mutations
{
    public static class AuthMutationType
    {
        public static void AddAuthMutations(this IObjectTypeDescriptor descriptor)
        {
            descriptor.Field("login")
                .Type<StringType>()
                .Argument("input", a => a.Type<NonNullType<LoginInputType>>())
                .Resolve(ctx =>
                {
                    var login = ctx.ArgumentValue<LoginModel>("input");

                    var response = ctx.Service<ILoginRepository>().Login(login);

                    return response;
                });
        }
    }
}
