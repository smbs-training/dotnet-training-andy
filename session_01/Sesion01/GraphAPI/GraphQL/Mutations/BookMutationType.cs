﻿using GraphAPI.GraphQL.Types;
using GraphAPI.Models;
using GraphAPI.Repositories.Interfaces;
using HotChocolate.Subscriptions;

namespace GraphAPI.GraphQL.Mutations
{
    public static class BookMutationType
    {
        public static void AddBookMutations(this IObjectTypeDescriptor descriptor)
        {
            descriptor.Field("addBook") 
                .Type<StringType>()
                .Argument("input", a => a.Type<NonNullType<CreateBookInputType>>())
                .Resolve(ctx =>
                {
                    var book = ctx.ArgumentValue<Book>("input");
                    
                    var sender = ctx.Service<ITopicEventSender>();

                    var response = ctx.Service<IBookRepository>().AddBook(book);

                    sender.SendAsync("BookAdded", response);

                    return response;
                });
        }
    }
}
