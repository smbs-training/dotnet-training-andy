﻿namespace GraphAPI.Models
{
    public class Author
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
