﻿namespace GraphAPI.Models
{
    public class Book
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual int AuthorId { get; set; }

        public virtual Author Author { get; set; }
    }
}
