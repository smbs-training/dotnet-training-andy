using GraphAPI.GraphQL.Mutations;
using GraphAPI.GraphQL.Queries;
using GraphAPI.GraphQL.Subscriptions;
using GraphAPI.NHibernate;
using GraphAPI.Repositories.Implementations;
using GraphAPI.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//Jwt configuration starts here
var jwtKey = builder.Configuration.GetSection("Jwt:Key").Get<string>() ?? string.Empty;

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidIssuer = "https://auth.chillicream.com",
        ValidAudience = "https://graphql.chillicream.com",
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey))
    };
});
//Jwt configuration ends here

// NHibernate
var connectionString = builder.Configuration.GetConnectionString("AppDbContext") ?? string.Empty;
builder.Services.AddNHibernate(connectionString);

builder.Services.AddScoped<ILoginRepository, LoginRepository>();

builder.Services.AddAuthorization();

builder.Services.AddScoped<IBookRepository, BookRepository>();
builder.Services.AddScoped<IAuthorRepository, AuthorRepository>();

builder.Services
    .AddGraphQLServer()
    .AddAuthorization()
    .RegisterService<IBookRepository>()
    .RegisterService<IAuthorRepository>()
    .AddQueryType<QueryType>()
    .AddMutationType<MutationType>()
    .AddSubscriptionType<SubscriptionType>()
    .AddInMemorySubscriptions()
    .AddFiltering()
    .AddSorting()
    .AddProjections();

//builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

//app.MapControllers();

app.UseWebSockets();

app.MapGraphQL();

app.Run();
