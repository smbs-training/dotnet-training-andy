﻿using GraphAPI.Models;

namespace GraphAPI.NHibernate
{
    public interface IMapperSession
    {
        void BeginTransaction();
        Task Commit();
        Task Rollback();
        void CloseTransaction();
        Task Save<T>(T entity);
        Task Delete<T>(T entity);
        Task RunInTransaction(Func<Task> action);
        Task<T> RunInTransaction<T>(Func<Task<T>> func);

        IQueryable<Book> Books { get; }
        IQueryable<Author> Authors { get; }
    }
}
