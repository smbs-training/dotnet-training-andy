﻿using GraphAPI.Models;
using NHibernate.Mapping.ByCode;
using NHibernate;
using NHibernate.Mapping.ByCode.Conformist;

namespace GraphAPI.NHibernate.Map
{
    public class AuthorMap : ClassMapping<Author>
    {
        public AuthorMap()
        {
            Id(x => x.Id, m =>
            {
                m.Generator(Generators.Identity);
                m.Type(NHibernateUtil.Int32);
                m.Column("Id");
                m.UnsavedValue(0);
            });

            Property(x => x.Name, m =>
            {
                m.Length(100);
                m.Type(NHibernateUtil.String);
                m.NotNullable(true);
            });

            Table("Authors");
        }
    }
}
