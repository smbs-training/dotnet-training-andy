﻿using GraphAPI.Models;
using NHibernate;
using ISession = NHibernate.ISession;

namespace GraphAPI.NHibernate
{
    public class NHibernateMapperSession : IMapperSession
    {
        private readonly ISession _session;
        private ITransaction? _transaction = null;

        public NHibernateMapperSession(ISession session)
        {
            _session = session;
        }

        // queries
        public IQueryable<Author> Authors => _session.Query<Author>();
        public IQueryable<Book> Books => _session.Query<Book>();
        // end queries

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            if (_transaction != null && _transaction.IsActive)
            {
                await _transaction.CommitAsync();
            }
        }

        public async Task Rollback()
        {
            if (_transaction != null && _transaction.IsActive)
            {
                await _transaction.RollbackAsync();
            }
        }

        public void CloseTransaction()
        {
            if (_transaction != null && _transaction.IsActive)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save<T>(T entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete<T>(T entity)
        {
            await _session.DeleteAsync(entity);
        }

        public async Task RunInTransaction(Func<Task> action)
        {
            using (var transaction = _session.BeginTransaction())
            {
                try
                {
                    await action();
                    await transaction.CommitAsync();
                }
                catch
                {
                    if (transaction.IsActive)
                    {
                        await transaction.RollbackAsync();
                    }
                    throw;
                }
            }
        }

        public async Task<T> RunInTransaction<T>(Func<Task<T>> func)
        {
            try
            {
                BeginTransaction();

                var retval = await func();

                await Commit();

                return retval;
            }
            catch
            {
                await Rollback();

                throw;
            }
            finally
            {
                CloseTransaction();
            }
        }
    }
}
