﻿using GraphAPI.Models;
using GraphAPI.Repositories.Interfaces;

namespace GraphAPI.Repositories.Implementations
{
    public class BookRepository : IBookRepository
    {
        public IQueryable<Book> GetBooks() {
            return new List<Book>
            {
                new Book
                {
                    Id = 1,
                    Title = "Book 1",
                    Author = new Author
                    {
                        Id = 1,
                        Name = "Author 1"
                    }
                },
                new Book
                {
                    Id = 2,
                    Title = "Book 2",
                    Author = new Author
                    {
                        Id = 2,
                        Name = "Author 2"
                    }
                },
                new Book
                {
                    Id = 3,
                    Title = "Book 3",
                    Author = new Author
                    {
                        Id = 3,
                        Name = "Author 3"
                    }
                }
            }.AsQueryable();
        }
        public Book GetBook()
        {
            return new Book
            {
                Id = 1,
                Title = "Book 1",
                Author = new Author
                {
                    Id = 1,
                    Name = "Author 1"
                }
            };
        }

        public string AddBook(Book book)
        {
            return book.Title;
        }
    }
}
