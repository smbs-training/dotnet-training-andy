﻿using GraphAPI.Models;
using GraphAPI.Repositories.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GraphAPI.Repositories.Implementations
{
    public class LoginRepository : ILoginRepository
    {
        private readonly IConfiguration _config;

        public LoginRepository(IConfiguration config)
        {
            _config = config;
        }

        public async Task<string> Login(LoginModel login)
        {
            var secretKey = _config.GetSection("Jwt:Key").Get<string>();

            if (string.IsNullOrEmpty(secretKey))
            {
                throw new Exception("Token configuration not found");
            }

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = new List<Claim>
             {
                 new Claim(ClaimTypes.GivenName, "User Test"),
                 new Claim(ClaimTypes.Email, login.Email)
             };

            var tokenDesc = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = credentials,
                Issuer = "https://auth.chillicream.com",
                Audience = "https://graphql.chillicream.com"
            };

            var token = tokenHandler.CreateToken(tokenDesc);

            return tokenHandler.WriteToken(token);
        }
    }
}
