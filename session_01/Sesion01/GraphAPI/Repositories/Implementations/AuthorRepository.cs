﻿using GraphAPI.Models;
using GraphAPI.NHibernate;
using GraphAPI.Repositories.Interfaces;
using NHibernate.Linq;

namespace GraphAPI.Repositories.Implementations
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly IMapperSession _session;

        public AuthorRepository(IMapperSession session)
        {
            _session = session;
        }

        public List<Author> GetAuthors()
        {
            //var models = _session.RunInTransaction(async () =>
            //{
            //    return await _session.Authors.ToListAsync();
            //});

            //return models.Result;

            return _session.Authors.ToList();
        }

        public Task<Author> GetAuthorAsync(int id)
        {
            var model = _session.RunInTransaction(async () =>
            {
                return await _session.Authors.FirstOrDefaultAsync(x => x.Id == id);
            });

            return model;
        }

        public async Task CreateAuthorAsync(Author author)
        {
            await _session.RunInTransaction(async () =>
            {
                await _session.Save(author);
            });
        }

        public async Task UpdateAuthorAsync(Author author)
        {
            await _session.RunInTransaction(async () =>
            {
                var existingAuthor = _session.Authors.First(x => x.Id == author.Id);
                existingAuthor.Name = author.Name;
                await _session.Save(existingAuthor);
            });
        }

        public async Task DeleteAuthorAsync(int id)
        {
            await _session.RunInTransaction(async () =>
            {
                var author = await GetAuthorAsync(id);
                await _session.Delete(author);
            });
        }
    }
}
