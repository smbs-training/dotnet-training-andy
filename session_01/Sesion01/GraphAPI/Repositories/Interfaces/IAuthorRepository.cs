﻿using GraphAPI.Models;

namespace GraphAPI.Repositories.Interfaces
{
    public interface IAuthorRepository
    {
        List<Author> GetAuthors();
        Task<Author> GetAuthorAsync(int id);
        Task CreateAuthorAsync(Author author);
        Task UpdateAuthorAsync(Author author);
        Task DeleteAuthorAsync(int id);
    }
}
