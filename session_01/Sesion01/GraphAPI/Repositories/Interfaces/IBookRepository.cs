﻿using GraphAPI.Models;

namespace GraphAPI.Repositories.Interfaces
{
    public interface IBookRepository
    {
        IQueryable<Book> GetBooks();
        Book GetBook();
        string AddBook(Book book);
    }
}
