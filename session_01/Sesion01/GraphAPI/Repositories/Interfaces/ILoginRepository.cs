﻿using GraphAPI.Models;

namespace GraphAPI.Repositories.Interfaces
{
    public interface ILoginRepository
    {
        Task<string> Login(LoginModel login);
    }
}
