﻿namespace Session03
{
    public interface IMessage
    {
        string MessageText { get; set; }
    }
}
