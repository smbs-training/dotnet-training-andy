﻿using System.ComponentModel.DataAnnotations;

namespace Session03.Models
{
    public class Book
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
    }
}
