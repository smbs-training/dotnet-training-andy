﻿namespace Session03
{
    public class Message: IMessage, IMessage2, IMessage3
    {
        public string MessageText { get; set; }

        public Message()
        {
            Random random = new Random();
            MessageText = random.Next().ToString();
        }
    }
}
