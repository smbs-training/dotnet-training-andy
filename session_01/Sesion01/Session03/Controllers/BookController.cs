﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Session03.Models;

namespace Session03.Controllers
{
    public class BookController : Controller
    {
        private readonly IMessage _message1;
        private readonly IMessage2 _message2;
        private readonly IMessage3 _message3;
        private readonly IMessage _message12;
        private readonly IMessage2 _message22;
        private readonly IMessage3 _message32;

        public BookController(
            IMessage message1, IMessage2 message2, IMessage3 message3,
            IMessage message12, IMessage2 message22, IMessage3 message32
            )
        {
            _message1 = message1;
            _message2 = message2;
            _message3 = message3;

            _message12 = message12;
            _message22 = message22;
            _message32 = message32;
        }

        // GET: BookController
        public ActionResult Index()
        {
            ViewData["Message1"] = _message1.MessageText;
            ViewData["Message2"] = _message2.MessageText;
            ViewData["Message3"] = _message3.MessageText;
            ViewData["Message12"] = _message12.MessageText;
            ViewData["Message22"] = _message22.MessageText;
            ViewData["Message32"] = _message32.MessageText;

            List<Book> books = new List<Book>
            {
                new Book { Title = "The Great Gatsby", Author = "F. Scott Fitzgerald", Year = 1925 },
                new Book { Title = "To Kill a Mockingbird"}
            };

            return View(books);
        }

        // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            Book book = new Book { Title = "The Great Gatsby", Author = "F. Scott Fitzgerald", Year = 1925 };
            return View(book);
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book book)
        {
            try
            {
                Console.WriteLine(book.Title);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
