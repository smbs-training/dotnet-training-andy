using Microsoft.AspNetCore.Mvc;

namespace Session04.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        //private static readonly string[] Summaries = new[]
        //{
        //    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        //};

        //private readonly ILogger<WeatherForecastController> _logger;

        private readonly Message _message;

        public WeatherForecastController(Message message)
        {
            _message = message;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public Message Get()
        {
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            //    TemperatureC = Random.Shared.Next(-20, 55),
            //    Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            //})
            //.ToArray();
            return _message;
        }

        [HttpGet(Name = "GetWeatherForecast2")]
        public Message Get2()
        {
            return _message;
        }
    }
}
