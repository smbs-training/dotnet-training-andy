﻿namespace Session04
{
    public class Message
    {
        public string MessageText { get; set; }

        public Message()
        {
            Random random = new Random();
            MessageText = random.Next().ToString();
        }   
    }
}
